use automerge;
use serde::{Deserialize, Serialize};

use autosurgeon::{Hydrate, Reconcile, hydrate, reconcile};

#[derive(Reconcile, Hydrate, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct PrimaryDocument {
    //pub control_plane_node: Vec<ControlPlaneNode>,
    //pub device: Vec<Device>,
    //pub device_auth_decision: Vec<DeviceAuthDecision>,
    //pub org_ipv6_range: Vec<OrgIpv6Range>,
    //pub organization: Vec<Organization>,
    pub sites: Vec<Site>,
    //pub site_range: Vec<SiteRange>,
    //pub user: Vec<User>,
    //pub user_session: Vec<UserSession>,
}


use std::fmt::Debug;
#[derive(Reconcile, Hydrate, Clone, Default, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Site {
    #[key]
    id: u64,
    name: String,
    // TODO - this should be a foreign key
    // TODO Can we go in reverse? Can this be a document with a list instead of forcing the
    // Lookup to be complicated?
    // pub controllers: Vec<u64>, // ControlPlaneNode //TODO --  Is this our ORM?
    // pub ranges: Vec<u64>, // SiteRange
}


fn main() {
    println!("Hello, world!");

    let mut hydrated_view = PrimaryDocument {
        sites: vec![
            Site {
                id: 1,
                name: "Lawnmower".to_string(),
            },
            Site {
                id: 2,
                name: "Strimmer".to_string(),
            },
        ]
    };

    // Put the hydrated_view into the document
    let mut doc = automerge::AutoCommit::new();
    reconcile(&mut doc, &hydrated_view).unwrap();
    println!("Doc 1: {:#?}", doc);

    // Fork the document and insert a new product at the start of the hydrated_view
    let mut doc2 = doc.fork().with_actor(automerge::ActorId::random());
    let mut hydrated_view2 = hydrated_view.clone();
    hydrated_view2.sites.insert(0, Site {
        id: 3,
        name: "Hat".to_string(),
    },);
    reconcile(&mut doc2, &hydrated_view2).unwrap();
    println!("Doc 2: {:#?}", doc2);

    // Concurrenctly remove a product from the hydrated_view in the original doc
    hydrated_view.sites.remove(0);
    reconcile(&mut doc, &hydrated_view).unwrap();

    // Merge the two changes
    doc.merge(&mut doc2).unwrap();

    println!("Doc 1: {:#?} {:#?}", doc, hydrated_view);


    let hydrated_2: PrimaryDocument = hydrate(&doc2).unwrap();
}
